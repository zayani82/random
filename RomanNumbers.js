function fromRoman(r)
{
let n=0,v,map={"I":1,"II":2,"III":3,"IV":4,"V":5,"VI":6,"VII":7,"VIII":8,"IX":9,
"X":10,"XX":20,"XXX":30,"XL":40,"L":50,"LX":60,"LXX":70,"LXXX":80,"XC":90,"C":100,
"CC":200,"CCC":300,"CD":400,"D":500,"DC":600,"DCC":700,"DCCC":800,"CM":900,"M":1000}; 

 whileloop:
 while(r.length)
 {
   for(i=4;i>0;i--) if(v=map[r.substr(0,i)]) {  n+=v; r=r.substr(i); continue whileloop;   }   
   return;
 }

return n;

}

function toRoman(n)
{
    let n1=",I,II,III,IV,V,VI,VII,VIII,IX,X".split(","),
        n10=",X,XX,XXX,XL,L,LX,LXX,LXXX,XC,C".split(","),
	 	n100=",C,CC,CCC,CD,D,DC,DCC,DCCC,CM".split(","),
        i,r="";
    i=Math.floor(n/1000);r="M".repeat(i); n-=i*1000;    
    i=Math.floor(n/100); r+=n100[i]; n-=i*100; 
    i=Math.floor(n/10); r+=n10[i]; n-=i*10;
    return r+=n1[n]; 
   
}